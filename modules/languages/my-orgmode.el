;;; my-orgmode.el --- Org-Mode Documentation Environment -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

(use-package org
  :ensure `(:repo "https://code.tecosaur.net/tec/org-mode.git" :branch "dev")
  :hook (org-mode . org-display-inline-images)
  :custom
  (org-directory "~/Workspace/private/memorandum/org-mode")
  (org-agenda-files (expand-file-name "agenda/init.org" org-directory))

  (org-link-frame-setup
   '((vm . vm-visit-folder-other-frame)
     (vm-imap . vm-visit-imap-folder-other-frame)
     (gnus . org-gnus-no-new-news)
     (file . find-file)
     (wl . wl-other-frame)))
  (org-catch-invisible-edits 'show-and-error)
  (org-cycle-include-plain-lists 'integrate)
  (org-cycle-separator-lines 2)
  (org-ellipsis "…")
  (org-export-coding-system 'utf-8)
  (org-export-preserve-breaks t)
  (org-fontify-quote-and-verse-blocks t)
  (org-hide-emphasis-markers t)
  (org-insert-heading-respect-content t)
  (org-special-ctrl-a/e t)
  (org-startup-indented t)
  (org-startup-with-inline-images t)
  (org-startup-with-latex-preview t)
  (org-image-actual-width nil)
  (org-support-shift-select t)
  (org-tags-column 0)
  (org-list-allow-alphabetical t)
  (org-pretty-entities t)
  (org-pretty-entities-include-sub-superscripts nil)

  ;; LaTeX
  (org-latex-tables-centered t)
  (org-highlight-latex-and-related '(native entities))

  ;; Code blocks
  (org-confirm-babel-evaluate nil)
  (org-edit-src-content-indentation 0)
  (org-edit-src-auto-save-idle-delay auto-save-timeout)
  (org-edit-src-turn-on-auto-save t)
  (org-src-fontify-natively t)
  (org-src-preserve-indentation t)
  (org-src-tab-acts-natively nil))

;; :NOTE| Control of Org source blocks and tangles

(use-feature ob
  :hook (org-babel-after-execute . org-display-inline-images)
  :custom
  (org-babel-default-header-args
   '((:async   . "yes")
     (:cache   . "no")
     (:eval    . "never-export")
     (:exports . "both")
     (:hlines  . "no")
     (:noweb   . "yes")
     (:results . "output replace")
     (:session . "none")
     (:tangle .  "no")))
  (org-export-use-babel nil)
  (org-confirm-babel-evaluate nil)
  :config
  ;; :NOTE| https://emacs.stackexchange.com/a/20618
  (defun demand-babel-languages (orig-fun &rest args)
    "Load language if needed before executing a source block."
    (let ((language (org-element-property :language (org-element-at-point))))
      (unless (cdr (assoc (intern language) org-babel-load-languages))
        (add-to-list 'org-babel-load-languages (cons (intern language) t))
        (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))
      (apply orig-fun args)))

  (defun irkalla/org-execute-action ()
    ;; In a source block, call `org-babel-execute-src-block'.
    (interactive)
    (let ((context (org-element-context)))
      (pcase (org-element-type context)
        (`src-block
         (org-babel-eval-wipe-error-buffer)
         (org-babel-execute-src-block current-prefix-arg))
        (`babel-call
         (call-interactively #'org-babel-execute-maybe))
        (`table-row
         (call-interactively #'org-table-next-row))
        ((or `link `timestamp)
         (call-interactively #'org-open-at-point))
        (_ (call-interactively #'forward-line)))))
  (advice-add 'org-babel-execute-src-block :around #'demand-babel-languages)

  (with-eval-after-load 'evil-collection
    (evil-collection-define-key 'normal 'org-mode-map
      (kbd "<return>") #'irkalla/org-execute-action)))

;; :NOTE| Auto-generated table of contents

(use-package toc-org
  :hook (org-mode . toc-org-mode)
  :custom (toc-org-max-depth 3))

;; :NOTE| Super-charge Org agendas

(use-package org-super-agenda
  :hook (org-agenda-mode . org-super-agenda-mode)
  :custom
  (org-super-agenda-groups
   '((:name "Today" :time-grid t :todo "TODAY")
     (:name "Important" :tag "bills" :priority "A")
     (:todo "WAITING" :order 8)
     (:todo ("SOMEDAY" "TO-READ" "CHECK" "TO-WATCH" "WATCHING") :order 9)
     (:priority<= "B" :order 1))))

;; :NOTE| Easier grep of Org files

(use-package org-ql
  :commands (org-ql-search))

;; :NOTE| insert a copy of text content via a file link or ID link

(use-package org-transclusion
  :after (org)
  :hook (org-mode . org-transclusion-mode))

;; :NOTE| Schedule your daily life through Org

(use-package org-timeblock
  :commands (org-timeblock))

;; :NOTE| Org based Zettlekasten System

(use-package org-roam
  :commands (org-roam-graph)
  :custom
  (org-roam-directory (expand-file-name "org-roam" org-directory))
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   `(("d" "default" plain "%?"
      :if-new (file+head
               "%<%Y%m%d%H%M%S>-${slug}.org"
               ,(let ((options '("#+options: _:{}"
                                 "#+options: ^:{}"
                                 "#+startup: latexpreview"
                                 "#+startup: entitiespretty"
                                 "#+startup: inlineimages"
                                 "#+title: ${title}")))
                  (mapconcat 'identity options "\n")))
      :unnarrowed t)))
  (org-roam-node-display-template "${title}"))

(use-package org-roam-ui
  :requires (org-roam)
  :commands (org-roam-ui-open)
  :custom
  (org-roam-ui-sync-theme t)
  (org-roam-ui-follow t)
  (org-roam-ui-update-on-save t)
  (org-roam-ui-open-on-start nil))

;; :NOTE| Cite your sources!!

(use-package citar
  :hook (org-mode . citar-capf-setup)
  :custom (citar-bibliography
           `(,(expand-file-name "references.bib" org-directory))))

(use-package citar-embark
  :requires (citar embark)
  :hook (org-mode . citar-embark-mode)
  :config (setopt citar-at-point-function 'embark-act))

;; :NOTE| LaTeX related customizations

(use-feature org-latex-preview
  :hook (org-mode . org-latex-preview-auto-mode)
  :config
  (plist-put org-latex-preview-appearance-options :scale 2.25)
  (plist-put org-latex-preview-appearance-options :zoom
             (- (/ (face-attribute 'default :height) 100.0) 0.025))
  :custom
  (org-preview-latex-image-directory (no-littering-expand-var-file-name "latex-preview/"))
  (org-latex-preview-live-debounce 0.25)
  (org-latex-preview-numbered t)
  (org-latex-preview-auto-ignored-commands
   '(next-line previous-line mwheel-scroll
               scroll-up-command scroll-down-command)))

;; :NOTE| Modernize the aesthetics of Org buffers

(use-package org-modern
  :after (nerd-icons)
  :hook
  ((org-mode . org-modern-mode)
   (org-agenda-finalize . org-modern-agenda))
  :custom
  (org-modern-fold-stars
   '((" 󰴈 " . " 󰴈 ") (" 󰊹 " . " 󰊹 ") (" 󰨑 " . " 󰨑 ") ("  " . "  ") ("  " . "  ")))

  (org-modern-keyword
   '(("options" . "  ") ("title" . "  ") ("author" . " 󱆀 ") ("email" . "  ")
     ("startup" . "  ") ("property" . "  ") ("date" . "  ") ("tags" . "  ")
     ("reveal" . " 󰐩 ") ("latex" . "  ") ("latex_header" . "  ")
     ("logbook" . "  ") ("language" . " 󰗊 ") ("todo" . "  ")
     (t . t)))

  ;; :NOTE| Settings replaced by svg-tag-mode
  (org-modern-tag nil)
  (org-modern-todo nil)
  (org-modern-block-name nil))

(provide 'my-orgmode)
