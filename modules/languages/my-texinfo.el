;;; my-texinfo.el --- TeXInfo Documentation Environment -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

(use-feature info
  :hook (Info-mode . hl-line-mode)
  :config
  (with-eval-after-load 'desktop
    (add-to-list 'desktop-modes-not-to-save 'Info-mode)
    (add-to-list 'desktop-modes-not-to-save 'info-lookup-mode))

  (when (fboundp 'visual-fill-column-mode)
    (setq-local fill-column 80)
    (add-hook 'Info-mode-hook #'visual-fill-column-mode)))

(provide 'my-texinfo)
