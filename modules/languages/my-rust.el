;;; my-rust.el --- Rust Development Environment -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

(use-package rustic
  :mode ("\\.rs\\'" . rustic-mode)
  :hook (rust-ts-mode . eglot-ensure)
  :custom
  (rustic-lsp-client 'eglot)
  (rustic-format-on-save nil)
  (rust-mode-treesitter-derive t)
  (rustic-cargo-use-last-stored-arguments t))

;; :NOTE| Adding a formatting option

(when (executable-find "rustfmt")
  (with-eval-after-load 'apheleia
    (setf (alist-get 'rust-ts-mode apheleia-mode-alist) '(rustfmt))))

;; :NOTE| Creating a hydra menu

(with-eval-after-load 'pretty-hydra
  (pretty-hydra-define rust-hydra
    (:title (pretty-hydra-title "──｢ Langspec: Rust ｣──" 'devicon "nf-dev-rust")
     :color teal :quit-key "q")
    ("Rustic"
     (("c" rustic-compile               "Compile")
      ("C" rustic-recompile             "Re-compile")
      ("r" rustic-cargo-run             "Run")
      ("b" rustic-cargo-build           "Build")
      ("T" rustic-cargo-test            "Tests")
      ("l" rustic-cargo-clippy          "Clippy")
      ("e" rustic-cargo-check           "Check for Errors"))
     "Cargo"
     (("a" rustic-cargo-add             "Add")
      ("x" rustic-cargo-rm              "Delete")
      ("c" rustic-cargo-clean           "Clean")
      ("h" rustic-cargo-doc             "Docs")
      ("B" rustic-cargo-bench           "Bench")
      ("u" rustic-cargo-update          "Update")
      ("o" rustic-cargo-outdated        "Outdated?"))
     "Actions"
     ((">" rust-promote-module-into-dir "Module -> Dir/mod.rs")
      ("p" rustic-playground            "Region -> Playground")
      ("P" rustic-playground-buffer     "Buf -> Playground"))))

  (pretty-hydra-define+ editor-hydra ()
    ("Programming"
     (("r" (if (memq major-mode '(rust-mode rust-ts-mode))
               (rust-hydra/body)
             (message "You are not in a rust buffer.")) "Rust")))))

(provide 'my-rust)
