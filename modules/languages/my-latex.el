;;; my-latex.el --- LaTeX Documentation Environment -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

(use-package auctex
  :mode ("\\.tex\\'" . LaTeX-mode)
  :custom
  (TeX-auto-save t)
  (TeX-parse-self t)
  (TeX-PDF-mode t)
  (TeX-file-line-error t)
  (TeX-process-asynchronous t)
  (TeX-source-correlate-start-server t)
  (preview-auto-cache-preamble t))

;; :NOTE| Fast input methods for entering LaTeX & math environments

(use-package cdlatex
  :hook ((LaTeX-mode . cdlatex-mode)
         (cdlatex-tab . yas-expand))
  :custom
  (cdlatex-auto-help-delay 0.2)
  (cdlatex-paired-parens "$[{(")
  (cdlatex-simplify-sub-super-scripts nil)
  (cdlatex-sub-super-scripts-outside-math-mode nil))

;; :NOTE| Easy array, matrix or table entry in LaTeX buffers

(use-package lazytab
  :ensure (:host github :repo "karthink/lazytab")
  :hook (LaTeX-mode . lazytab-mode))

(provide 'my-latex)
