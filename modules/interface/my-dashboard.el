;;; my-dashboard.el --- Emacs Startup Buffer -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

;; :WARN| Without dashboard -> startup-time < 0.3Xs.

(use-package dashboard
  :after (nerd-icons)
  :commands (dashboard-open)
  :hook (dashboard-mode . hl-line-mode)
  :config
  (when (fboundp 'visual-fill-column-mode)
    (add-hook 'dashboard-mode-hook #'visual-fill-column-mode))

  (when (fboundp 'page-break-lines-mode)
    (setopt dashboard-page-separator "\n\f\n")
    (add-hook 'dashboard-mode-hook #'page-break-lines-mode))
  :custom
  (dashboard-display-icons-p t)
  (dashboard-icon-type 'nerd-icons)
  (dashboard-banner-logo-title "Welcome to the underworld, human.")
  (dashboard-modify-heading-icons '((recents . "file-text")
                                    (bookmarks . "book")))
  (dashboard-startup-banner
   (expand-file-name "logos/svg/lotus.svg" irkalla/underworld))
  (dashboard-path-max-length 20)
  (dashboard-set-heading-icons t)
  (dashboard-set-file-icons t)
  (dashboard-set-init-info t)
  (dashboard-week-agenda t)
  (dashboard-set-navigator t)
  (dashboard-items '((recents . 5)
                     (bookmarks . 5)))
  (dashboard-item-names
   '(("Recent Files:" . " Recently opened files:")
     ("Bookmarks:"    . " Pinned Items:"))))

(provide 'my-dashboard)
