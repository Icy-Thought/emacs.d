;;; my-modeline.el --- A Status-Bar For Emacs -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

(use-package mini-echo
  :hook (emacs-startup . mini-echo-mode)
  :custom
  (mini-echo-persistent-rule
   '(:long ("macro" "evil" "popper"
            "buffer-name" "project" "vcs" "major-mode"
            "eglot" "flymake" "envrc")
     :short ("evil" "popper" "buffer-name" "project" "eglot")))
  (mini-echo-update-interval 0)
  (mini-echo-separator (propertize "  " 'face 'window-divider))
  (mini-echo-ellipsis "…")
  (mini-echo-right-padding 1)
  (mini-echo-buffer-status-style 'both)
  (mode-line-position-column-line-format '("%l:%c,%p")))

(provide 'my-modeline)
