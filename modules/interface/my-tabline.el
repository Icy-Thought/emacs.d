;;; my-tabline.el --- A Bar For The Ever-Growing Buffers -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

(use-feature tab-line
  :preface
  (defcustom irkalla/tab-line-excluded-patterns nil
    "A list of excluded buffers from `tab-line' based on `rx' patterns."
    :type '(repeat regexp)
    :group 'irkalla)

  (defun irkalla/tab-line-filtered-buffers ()
    "Filter buffers for `tab-line' based on excluded `rx' patterns."
    (seq-filter (lambda (buffer)
                  (let ((name (buffer-name buffer)))
                    (not (seq-some (lambda (pattern) (string-match-p pattern name))
                                   irkalla/tab-line-excluded-patterns))))
                (tab-line-tabs-mode-buffers)))

  (defun irkalla/tab-line-display-rules ()
    "Toggle the visibility of tab-line based on `tab-line-tabs-function'."
    (setq tab-line-format
          (when (and (boundp 'tab-line-tabs-function)
                     (functionp tab-line-tabs-function)
                     (funcall tab-line-tabs-function))
            tab-line-format)))
  :bind (:map tab-line-mode-map
              ("M-<prior>" . tab-line-switch-to-prev-tab)
              ("M-<next>" . tab-line-switch-to-next-tab))
  :hook
  ((emacs-startup . global-tab-line-mode)
   (buffer-list-update . irkalla/tab-line-display-rules)
   (window-configuration-change . irkalla/tab-line-display-rules))
  :custom
  (irkalla/tab-line-excluded-patterns
   `(,(rx (or "*" " *"))
     ,(rx "consult-partial-preview")
     ,(rx (seq "Ement"))
     ,(rx (seq "magit"))))

  (tab-line-tabs-buffer-group-sort-function nil)
  (tab-line-tabs-function #'irkalla/tab-line-filtered-buffers))

(use-package tab-line-nerd-icons
  :hook (tab-line-mode . tab-line-nerd-icons-global-mode)
  :custom
  (tab-line-nerd-icons-extra-raise -0.05)
  (tab-line-nerd-icons-space-width 1.5)
  (tab-line-nerd-icons-base-icon-height 1.05))

(provide 'my-tabline)
