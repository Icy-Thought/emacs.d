;;; my-core.el --- The Core of Irkalla Emacs -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

(use-feature emacs
  :hook (emacs-startup . pixel-scroll-precision-mode)
  :custom
  (confirm-kill-processes nil)
  (large-file-warning-threshold (* 55 1080 1080))
  (scroll-preserve-screen-position t))

(use-feature time
  :custom
  (display-time-24hr-format t)
  (display-time-day-and-date t))

(use-feature calendar
  :custom
  (calendar-date-style 'european)
  (calendar-week-start-day 1)
  (calendar-intermonth-text
   '(propertize
     (format "%2d"
             (car (calendar-iso-from-absolute
                   (calendar-absolute-from-gregorian (list month day year)))))
     'font-lock-face 'calendar-weekend-header)))

;; :NOTE| Time to require the modules

(require 'my-windows)
(require 'my-littering)
(require 'my-hydra)

(provide 'my-core)
