;;; my-windows.el --- Control of Emacs Windows -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

;; :NOTE| Window navigation ought to be a simple task!

(use-feature windmove
  :config
  (windmove-default-keybindings)
  (windmove-default-keybindings 'meta))

;; :NOTE| Toggle focus/unfocus of certain buffers on demand

(use-feature winner
  :hook (emacs-startup . winner-mode))

;; :NOTE| Clear dividers for split buffers

(use-feature frame
  :hook (emacs-startup . window-divider-mode)
  :custom
  (window-divider-default-places t)
  (window-divider-default-right-width 2)
  (window-divider-default-bottom-width 2))

;; :NOTE| Control window placement through rules

(use-package shackle
  :hook (emacs-startup . shackle-mode)
  :custom
  (shackle-default-size 0.33)
  (shackle-rules
   `((help-mode                                   :align right :select t :size 0.45)
     (helpful-mode                                :align right :select t :size 0.45)
     (compilation-mode                            :align right)
     (,(rx "*" (* any) "compilation" (* any) "*") :align right :regexp t)
     (flymake-diagnostics-buffer-mode             :align below)
     (magit-process-mode                          :align below)
     ("*eldoc*"                                   :align right)
     ("*Messages*"                                :align below)
     ("*Async-native-compile-log*"                :align right)
     ("*devdocs*"                                 :align right :select t :same t :inhibit-window-quit t)
     ("*mu4e-headers*"                            :align right :select t :size 0.75)
     ;; also launch without invoking J -> inbox manual select -> head into inbox by defeault
     (,(rx "*" (* any) "REPL" (* any) "*")        :align right :regexp t)
     (,(rx bos "*" (* any)
           (| "eat" "eshell" "shell" "term" "vterm")
           (* any) "*" eos)
      :align below :select t :regexp t :size 0.45))))

;; :NOTE| Morph buffers into scratchpads

(use-package popper
  :hook (shackle-mode . popper-mode)
  :config
  (with-eval-after-load 'project
    (setopt popper-group-function #'popper-group-by-project))
  :custom
  (popper-display-control nil)
  (popper-echo-dispatch-keys nil)
  (popper-reference-buffers
   `(help-mode helpful-mode
     ,(rx "*Messages*")
     ,(rx "Output*" eos)
     ,(rx "*devdocs*")
     ,(rx "*" (* any) "REPL" (* any) "*")
     compilation-mode magit-process-mode
     eat-mode eshell-mode shell-mode term-mode vterm-mode)))

(provide 'my-windows)
