;;; my-coding.el --- Creating A Development Environment -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

(use-feature emacs
  :hook (compilation-filter . ansi-color-compilation-filter)
  :custom
  (tab-always-indent 'complete)
  (compilation-scroll-output t)
  (compilation-skip-visited t)
  (compilation-always-kill t)
  (read-buffer-completion-ignore-case t)
  (read-file-name-completion-ignore-case t)
  (read-extended-command-predicate #'command-completion-default-include-p))

;; :NOTE| Emacs built-in Language server protocol

(use-feature eglot
  :defer t
  :config (advice-add 'jsonrpc--log-event :override #'ignore)
  :custom
  (eglot-menu-string "</>")
  (eglot-autoshutdown t)
  (eglot-sync-connect 0)
  (eglot-extend-to-xref t)
  (eglot-confirm-server-initiated-edits nil)
  (eglot-ignored-server-capabilities '(:documentHighlightProvider))
  (eglot-events-buffer-config '(:size 0)))

;; :NOTE| Boosting Eglot's ego to make it faster

(use-package eglot-booster
  :ensure (:host github :repo "jdtsmith/eglot-booster")
  :if (executable-find "emacs-lsp-booster")
  :after (eglot)
  :config (eglot-booster-mode +1)
  :custom (eglot-booster-io-only t))

;; :NOTE| Complation candidate matching

(use-package orderless
  :custom
  (completions-detailed t)
  (completion-ignore-case t)
  (completion-styles '(orderless basic))
  (completion-category-defaults nil)
  (completion-category-overrides '((file (styles basic partial-completion)))))

;; :NOTE| Documentation of a given $SYMB

(use-feature eldoc
  :diminish (eldoc-mode)
  :config
  (remove-hook 'eldoc-display-functions 'eldoc-display-in-echo-area)
  :custom
  (eldoc-idle-delay 1.0)
  (eldoc-echo-area-display-truncation-message nil)
  (eldoc-echo-area-use-multiline-p nil)
  (eldoc-echo-area-prefer-doc-buffer t)
  (eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly))

(use-package eldoc-box
  :commands (eldoc-box-help-at-point)
  :init
  (with-eval-after-load 'evil-collection
    (evil-collection-define-key 'normal 'prog-mode-map
      (kbd "M-TAB") #'eldoc-box-help-at-point
      (kbd "M-j") #'eldoc-box-scroll-up
      (kbd "M-j") #'eldoc-box-scroll-down)))

;; :NOTE| Controlling suggestion outputs

(use-package cape
  :init
  (add-hook 'prog-mode-hook
            (lambda ()
              (add-hook 'completion-at-point-functions #'cape-file nil t)
              (add-hook 'completion-at-point-functions #'cape-keyword nil t)
              (add-hook 'completion-at-point-functions #'cape-elisp-symbol nil t)
              (add-hook 'completion-at-point-functions #'cape-tex nil t))))

;; :NOTE| Built-in buffer diagnostics

(use-feature flymake
  :commands (flymake-mode)
  :custom
  (flymake-indicator-type 'margins)
  (flymake-margin-indicator-position 'right-margin)
  (flymake-autoresize-margins t)
  (flymake-margin-indicators-string
   '((error   " " compilation-error)
     (warning " " compilation-warning)
     (note    " " compilation-info))))

;; :NOTE| Elegant completion UI

(use-package corfu
  :ensure (:files (:defaults "extensions/*.el"))
  :hook (emacs-startup . global-corfu-mode)
  :custom
  (corfu-auto t)
  (corfu-auto-delay 0)
  (corfu-auto-prefix 2)
  (corfu-on-exact-match 'show))

(use-feature corfu-popupinfo
  :bind (:map corfu-popupinfo-map
              ("M-TAB" . corfu-popupinfo-toggle)
              ("M-k" . corfu-popupinfo-scroll-down)
              ("M-j" . corfu-popupinfo-scroll-up))
  :hook (corfu-mode . corfu-popupinfo-mode)
  :custom (corfu-popupinfo-delay nil))

;; :NOTE| Completion through abbreviations

(use-package yasnippet
  :diminish (yas-minor-mode)
  :bind ("M-]" . yas-insert-snippet)
  :hook (emacs-startup . yas-global-mode)
  :init
  (add-to-list 'yas-snippet-dirs
               (expand-file-name "snippets" irkalla/underworld))
  :config
  ;; :WARN| Complaints when using / -> \frac{}{} otherwise

  (add-to-list 'warning-suppress-types '(yasnippet backquote-change))

  ;; https://github.com/joaotavora/yasnippet/issues/998#issuecomment-496449546
  (defun irkalla/yas-auto-expanding-snippets ()
    (when (and (boundp 'yas-minor-mode) yas-minor-mode)
      (let ((yas-buffer-local-condition ''(require-snippet-condition . auto)))
        (yas-expand))))
  (add-hook 'post-command-hook #'irkalla/yas-auto-expanding-snippets)
  :custom (yas-triggers-in-field t))

(use-package yasnippet-snippets
  :after (yasnippet))

(use-package yasnippet-capf
  :after (yasnippet)
  :config
  (add-hook 'completion-at-point-functions #'yasnippet-capf)
  :custom (yasnippet-capf-lookup-by 'name))

;; :NOTE| Debug adapter protocol

(use-package dape
  :commands (dape)
  :custom
  (dape-key-prefix "\C-x\C-a")
  (dape-buffer-window-arrangement 'right)
  (dape-cwd-fn 'project-vc-extra-root-markers))

;; :NOTE| A formatter for our messy code

(use-package apheleia
  :commands (apheleia-format-buffer))

;; :NOTE| Language based documentations

(use-package devdocs
  :commands (devdocs-install devdocs-lookup)
  :hook (devdocs-mode . visual-wrap-prefix-mode))

;; :NOTE| Programming problems that remains to be solved

(use-package leetcode
  :commands (leetcode)
  :custom
  (leetcode-save-solutions t)
  (leetcode-prefer-language "python3")
  (leetcode-directory (no-littering-expand-var-file-name "leetcode/")))

(provide 'my-coding)
