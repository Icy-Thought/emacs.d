;;; my-frames.el --- Rules For Emacs Frames -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

;; :NOTE| Emacs session management

(use-feature desktop
  :hook (prog-mode . desktop-save-mode)
  :custom
  (desktop-modes-not-to-save
   '(fundamental-mode dired-mode tags-table-mode))
  (desktop-base-file-name "last-session")
  (desktop-base-lock-name (concat desktop-base-file-name ".lock"))
  (desktop-restore-eager 25)
  (desktop-file-checksum t)
  (desktop-save-buffer t)
  (desktop-save t))

;; :NOTE| Management of random ideas

(use-feature project
  :custom
  (project-vc-extra-root-markers
   '(".repo" ".dir-locals.el" ".project" ".project.el" ".projectile.el"
     "Makefile" "CMakeLists.txt" "xmake.lua"
     "Cargo.toml"
     "Dockerfile" "autogen.sh"
     "package.json" "requirements.txt")))

(provide 'my-frames)
