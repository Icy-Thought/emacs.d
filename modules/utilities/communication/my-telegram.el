;;; my-telegram.el --- A client for Telegram -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

(use-package telega
  :commands (telega)
  :hook (telega-load-hook . (lambda ()
                              (telega-notifications-mode)
                              (telega-appindicator-mode)))
  :config
  (when (fboundp 'visual-fill-column-mode)
    (remove-hook 'telega-chat-mode-hook #'telega-chat-auto-fill-mode)
    (add-hook 'telega-chat-mode-hook #'visual-fill-column-mode))

  ;; :NOTE| Enable dictionary + emoji suggestions in compose area
  (with-eval-after-load 'cape
    (add-hook 'telega-chat-mode-hook
              (lambda () (add-hook 'completion-at-point-functions #'cape-emoji nil t))))
  :custom
  (telega-server-libs-prefix (getenv "TDLIB_PREFIX"))
  (telega-directory (no-littering-expand-var-file-name "telega/"))
  (telega-use-images t)
  (telega-chat-show-avatars t)
  (telega-root-show-avatars t)
  (telega-user-show-avatars t)
  (telega-emoji-font-family
   (when (featurep 'fontaine)
     (plist-get (fontaine--get-preset-properties 'default) :default-family)))
  (telega-emoji-use-images nil) ;; :WARN| libsvg issue -> odd symbols
  (telega-chat-bidi-display-reordering t))

(provide 'my-telegram)
