;;; my-dired.el --- Interactive File/Directory Viewer -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

(use-feature dired
  :custom
  (dired-auto-revert-buffer t)
  (dired-mouse-drag-files t)
  (dired-kill-when-opening-new-dired-buffer t)
  (mouse-drag-and-drop-region-cross-program t)
  (mouse-1-click-follows-link nil)
  (dired-movement-style 'cycle)
  (dired-listing-switches "-alFh --group-directories-first"))

;; :NOTE| Controlling various file extensions

(use-feature dired-x
  :preface
  (defun dired-external-launch (application extensions)
    "External `APPLICATION' used for launching specific file-extensions."
    (let ((pattern (rx "." extensions eos))
          (entry (list pattern application)))
      (add-to-list 'dired-guess-shell-alist-user entry)))
  :custom
  (dired-external-launch
   (if (eq system-type 'gnu/linux) "mpv" "xdg-open")
   '("avi" "flv" "mkv" "mov" "mp3" "mp4" "mpeg" "mpg" "ogg" "ogm" "wav" "wmv"))

  (dired-external-launch
   (if (eq system-type 'gnu/linux) "libreoffice" "xdg-open")
   '("doc" "docx"  "odt" "xls" "xlsx")))

;; :NOTE| Directories should have some form of highlighting

(use-package diredfl
  :hook ((dired-mode dirvish-directory-view-mode) . diredfl-mode))

;; :NOTE| Polished batteries for Dired

(use-package dirvish
  :after (dired)
  :config
  (dirvish-override-dired-mode)
  (dirvish-side-follow-mode)
  :custom
  (dirvish-side-width 30)
  (dirvish-use-header-line t)
  (dirvish-use-mode-line nil)
  (dirvish-fd-default-dir "~/")
  (dirvish-quick-access-entries
   '(("h" "~/"                          "Home")
     ("d" "~/Downloads/"                "Downloads")
     ("m" "/mnt/"                       "Drives")
     ("l" "~/Library/unexplored"        "Library")
     ("t" "~/.local/share/Trash/files/" "Rubbish Bin")))
  (dirvish-attributes '(nerd-icons file-size collapse subtree-state vc-state))
  (dired-listing-switches "-l --almost-all --human-readable --group-directories-first --no-group"))

(provide 'my-dired)
