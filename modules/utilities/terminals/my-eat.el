;;; my-eat.el --- Elisp Based Terminal Emulator -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

(use-package eat
  :commands (eat eat-project)
  :hook (eshell-load . eat-eshell-mode)
  :custom
  (eat-kill-buffer-on-exit t)
  (eat-eshell-visual-command-mode '())
  (eat-enable-shell-prompt-annotation nil))

(provide 'my-eat)
