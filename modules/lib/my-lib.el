;;; my-lib.el --- Useful Consts & Functions & Macros -*- lexical-binding: t; -*-

;; Copyright (C) 2023-2025 Icy-Thought

;; Author: Icy-Thought <icy-thought@pm.me>
;; Keywords: internal
;; URL: https://github.com/Icy-Thought/emacs.d/

;; :NOTE| Quick-access to built-in & Emacs pkgs from nixpkgs

(defmacro use-feature (name &rest args)
  "`use-package', but for built-in packages."
  (declare (indent defun))
  `(use-package ,name
     :ensure nil
     ,@args))

;; :NOTE| Determine what system Emacs is installed in

(defconst IS-DECLERATIVE?
  (and (eq system-type 'gnu/linux)
       (with-temp-buffer
         (insert-file-contents "/etc/os-release")
         (re-search-forward
          (rx bol "ID=" (group (| "nixos" "guix")) eol) nil t))))

;; :NOTE| Read secrets from their designaed directory

(defvar irkalla/password-manager-directory "/run/agenix")

(defun irkalla/fetch-password (filename)
  "Fetch secret content from the designated file using the specified password manager."
  (let* ((password-directory irkalla/password-manager-directory)
         (password-path (expand-file-name filename password-directory)))
    (unless password-directory
      (error "Password '%s' not found in `irkalla/password-manager-directory'" filename))
    (with-temp-buffer
      (insert-file-contents password-path)
      (string-trim-right (buffer-string)))))

;; :NOTE| Functions for easier system-clipboard access

(defun irkalla/copy-to-sysclip ()
  "Copy contents to the system clipboard."
  (interactive)
  (setopt select-enable-clipboard t)
  (if (featurep 'evil)
      (call-interactively #'evil-yank)
    (kill-ring-save (region-beginning) (region-end)))
  (setopt select-enable-clipboard nil))

(defun irkalla/paste-from-sysclip ()
  "Paste contents to the system clipboard."
  (interactive)
  (setopt select-enable-clipboard t)
  (if (featurep 'evil)
      (call-interactively #'evil-paste-after)
    (yank))
  (setopt select-enable-clipboard nil))

;; :NOTE| Startup time is crusial, thus we need something to help us measure it!

(defmacro with-timer (name &rest body)
  `(let ((time (current-time)))
 ,@body
 (message "%s: %.06f" ,name (float-time (time-since time)))))

;; :NOTE| Time to require the modules

(require 'my-custom-modes)

(provide 'my-lib)
